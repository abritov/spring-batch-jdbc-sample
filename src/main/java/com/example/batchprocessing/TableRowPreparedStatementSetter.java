package com.example.batchprocessing;

import org.springframework.batch.item.database.ItemPreparedStatementSetter;
import org.springframework.jdbc.core.SqlParameterValue;
import org.springframework.jdbc.core.StatementCreatorUtils;

import java.sql.PreparedStatement;
import java.sql.SQLException;

public class TableRowPreparedStatementSetter implements ItemPreparedStatementSetter<SqlParameterValue[]> {

    @Override
    public void setValues(SqlParameterValue[] values, PreparedStatement preparedStatement) throws SQLException {
        for (int i = 0; i<values.length; i++) {
            StatementCreatorUtils.setParameterValue(preparedStatement, i+1, values[i], values[i].getValue());
        }
    }

}
