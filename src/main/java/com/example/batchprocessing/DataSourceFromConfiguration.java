package com.example.batchprocessing;

import org.springframework.boot.context.properties.ConfigurationProperties;
import org.springframework.context.annotation.Configuration;

@Configuration
@ConfigurationProperties("spring.datasource-from")
public class DataSourceFromConfiguration extends EtlDataSourceConfiguration{

}
