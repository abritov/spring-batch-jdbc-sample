package com.example.batchprocessing;

import com.zaxxer.hikari.HikariConfig;
import com.zaxxer.hikari.HikariDataSource;
import org.springframework.batch.core.Job;
import org.springframework.batch.core.JobParameters;
import org.springframework.batch.core.Step;
import org.springframework.batch.core.job.builder.JobBuilder;
import org.springframework.batch.core.launch.JobLauncher;
import org.springframework.batch.core.launch.support.RunIdIncrementer;
import org.springframework.batch.core.repository.JobRepository;
import org.springframework.batch.core.step.builder.StepBuilder;
import org.springframework.batch.item.database.JdbcBatchItemWriter;
import org.springframework.batch.item.database.JdbcCursorItemReader;
import org.springframework.batch.item.database.builder.JdbcBatchItemWriterBuilder;
import org.springframework.batch.item.database.builder.JdbcCursorItemReaderBuilder;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.ApplicationArguments;
import org.springframework.boot.ApplicationRunner;
import org.springframework.jdbc.core.SqlParameterValue;
import org.springframework.jdbc.support.JdbcTransactionManager;
import org.springframework.stereotype.Component;

import javax.sql.DataSource;
import java.util.function.Function;

@Component
public class ApplicationLoader implements ApplicationRunner {

    @Autowired
    private DataSourceFromConfiguration dataSourceFromConfiguration;

    @Autowired
    private DataSourceToConfiguration dataSourceToConfiguration;

    @Autowired
    private JobLauncher jobLauncher;

    @Autowired
    private JobRepository jobRepository;

    @Autowired
    private BatchConfiguration batchConfiguration;

    @Override
    public void run(ApplicationArguments args) throws Exception {
        final DataSource dataSourceFrom = getHikariDatasource(dataSourceFromConfiguration);
        final DataSource dataSourceTo = getHikariDatasource(dataSourceToConfiguration);
        dataSourceFrom.getConnection();
        dataSourceTo.getConnection();
        final JdbcCursorItemReader<SqlParameterValue[]> reader = new JdbcCursorItemReaderBuilder<SqlParameterValue[]>()
                .rowMapper(new TableRowObjectMapper())
                .dataSource(dataSourceFrom)
                .sql(batchConfiguration.getSelectStatement())
                .verifyCursorPosition(false)
                .fetchSize(batchConfiguration.getFetchSize())
                .connectionAutoCommit(false)
                .name("source")
                .build();
        final JdbcBatchItemWriter<SqlParameterValue[]> writer = new JdbcBatchItemWriterBuilder<SqlParameterValue[]>()
                    .itemPreparedStatementSetter(new TableRowPreparedStatementSetter())
                    .sql(batchConfiguration.getInsertStatement())
                    .dataSource(dataSourceTo)
                    .build();
        final JdbcTransactionManager dataSourceTransactionManager = new JdbcTransactionManager();
        dataSourceTransactionManager.setDataSource(dataSourceTo);
        final Step step = new StepBuilder("step")
                    .repository(jobRepository)
                    .transactionManager(dataSourceTransactionManager)
                    .<SqlParameterValue[], SqlParameterValue[]> chunk(batchConfiguration.getChunkSize())
                    .reader(reader)
                    .processor((Function<? super SqlParameterValue[], ? extends SqlParameterValue[]>) i -> i)
                    .writer(writer)
                    .build();
//        Boot 3
/*
        final Step step = new StepBuilder("step", jobRepository)
                    .<SqlParameterValue[], SqlParameterValue[]> chunk(batchConfiguration.getChunkSize(), transactionManager)
                    .reader(reader)
                    .processor(i -> i)
                    .writer(writer)
                    .build();
*/
        final Job job = new JobBuilder("job")
                    .repository(jobRepository)
                    .incrementer(new RunIdIncrementer())
                    .flow(step)
                    .end()
                    .build();
        //  Boot 3
/*
         final Job job = new JobBuilder("job", jobRepository)
                .incrementer(new RunIdIncrementer())
                .flow(step)
                .end()
                .build();
*/
        jobLauncher.run(job, new JobParameters());
    }

    private DataSource getHikariDatasource(final EtlDataSourceConfiguration configuration) {
        final HikariConfig hikariConfig = new HikariConfig();
        hikariConfig.setDriverClassName(configuration.getDriverClassName());
        hikariConfig.setJdbcUrl(configuration.getJdbcUrl());
        hikariConfig.setUsername(configuration.getUsername());
        hikariConfig.setPassword(configuration.getPassword());
        if (configuration.getInitSql() != null && !configuration.getInitSql().isEmpty())
            hikariConfig.setConnectionInitSql(configuration.getInitSql());
        return new HikariDataSource(hikariConfig);
    }

}
