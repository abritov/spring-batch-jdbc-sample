package com.example.batchprocessing;


import org.springframework.jdbc.core.RowMapper;
import org.springframework.jdbc.core.SqlParameterValue;
import org.springframework.jdbc.support.JdbcUtils;

import java.sql.ResultSet;
import java.sql.ResultSetMetaData;
import java.sql.SQLException;

public class TableRowObjectMapper implements RowMapper<SqlParameterValue[]> {

    @Override
    public SqlParameterValue[] mapRow(ResultSet rs, int rowNum) throws SQLException {
        final ResultSetMetaData metaData = rs.getMetaData();
        final int size = metaData.getColumnCount();
        final SqlParameterValue[] result = new SqlParameterValue[size];
        for (int i = 0; i<size; i++ ) {
            final int columnType = metaData.getColumnType(i+1);
            result[i] = new SqlParameterValue(columnType, JdbcUtils.getResultSetValue(rs, i+1));
        }
        return result;
    }

}
