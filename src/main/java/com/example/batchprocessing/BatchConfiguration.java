package com.example.batchprocessing;

import org.springframework.batch.core.configuration.annotation.EnableBatchProcessing;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.Configuration;


@Configuration
@EnableBatchProcessing // Disable for boot 3
public class BatchConfiguration {

	@Value("${fetchSize:1000}")
	private int fetchSize;

	@Value("${chunkSize:10000}")
	private int chunkSize;

	@Value("${selectStatement:SELECT '2'}")
	private String selectStatement;

	@Value("${insertStatement:}")
	private String insertStatement;

	public int getFetchSize() {
		return fetchSize;
	}

	public int getChunkSize() {
		return chunkSize;
	}

	public String getSelectStatement() {
		return selectStatement;
	}

	public String getInsertStatement() {
		return insertStatement;
	}

}
